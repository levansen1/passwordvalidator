/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package passwordprotector;

import java.util.regex.*;
public class PasswordValidator {
    private String password;
   
   
   PasswordValidator(){
       
   }
   
    PasswordValidator(String password){
        this.password = password;
  
    }
    
    public boolean ValidatePassword(String pass){
        
        validateStringLen(pass);
        validateChar(pass);
        validateNumber(pass);
       
       if(validateStringLen(pass) && validateChar(pass) && validateNumber(pass)){
           return true;
       }
       return false;
    }
    
    public Boolean validateStringLen(String pass){
        
        if(pass.length() >=8){
            return true;
        }
        return false;
    }
    
     public Boolean validateChar(String pass){
       
         boolean containsAnUpperCase=false;
         for (int i = 0; i < pass.length(); i++) {
    if (Character.isUpperCase(pass.charAt(i))) {
      containsAnUpperCase = true;
    }
  }
         
         Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
         Matcher m = p.matcher(pass);
         boolean hasChar = m.find();
         
         if(hasChar==true && containsAnUpperCase==true){
             return true;
         }
         
        return false;
    }
     
      public Boolean validateNumber(String pass){
          char[] ch = pass.toCharArray();
          for(char c : ch){
              if(Character.isDigit(c)){
                  return true;
              }
          }
        return false;
    }
      
      String setPassword(String password){
          this.password = password;
          return password;
      }
    
}
